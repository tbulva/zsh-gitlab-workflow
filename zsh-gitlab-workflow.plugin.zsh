affected_files() {
  git --no-pager diff --name-only $(git rev-parse HEAD) $(git rev-parse HEAD~1)
}

affected_files_pretty_print() {
  affected_files | tr " " "\n"
}

has_affected_files() {
  if [ -z "$(affected_files)" ] || [ -z "$1" ]; then
    printf " \xF0\x9F\x8F\x83 SKIPPING\n"
  else
    printf "\xE2\x9C\x85\n"
    sh -c $1
    printf "\n"
  fi
}

affected_files_scss() {
  git --no-pager diff --name-only $(git rev-parse HEAD) $(git rev-parse HEAD~1) | grep '.css\|scss'
}

has_affected_files_scss() {
  if [ -z "$(affected_files_scss)" ] || [ -z "$1" ]; then
    printf " \xF0\x9F\x8F\x83 SKIPPING\n"
  else
    printf "\xE2\x9C\x85\n"
    sh -c $1
    printf "\n"
  fi
}

affected_files_js() {
  git --no-pager diff --name-only $(git rev-parse HEAD) $(git rev-parse HEAD~1) | grep '.js\|.vue\|.graphql'
}

has_affected_files_js() {
  echo $1
  if [ -z "$(affected_files_js)" ] || [ -z "$1" ]; then
    printf " \xF0\x9F\x8F\x83 SKIPPING\n"
  else
    printf "\xE2\x9C\x85\n"
    sh -c $1
    printf "\n"
  fi
}

lint_all() {
  yarn run lint:prettier:fix
  yarn run lint:eslint:all:fix
}

lint_css() {
  yarn run stylelint -q $(affected_files_scss)
}

lint_staged() {
  printf "\nlint:"
  has_affected_files_js "yarn run internal:eslint $(affected_files_js) --fix"
  
  printf "\nchecking styles"
  has_affected_files_scss "lint_css"

  printf "\nprettier"
  has_affected_files_js "yarn run prettier --check $(affected_files_js)"

  printf "\n\n"
}

generate_translation() {
  bin/rake gettext:regenerate
}

force_push() {
  if [[ $(ag console.log $(affectedfiles)) ]]; then
    echo "console.log found in affected files"
    ag console.log $(affectedfiles)
  else
    lint_staged && ggpush --force
  fi
}

generate_startup_css() {
  bundle exec rspec spec/frontend/fixtures/startup_css.rb
  yarn run generate:startup_css
}

gl_help() {
  echo "Usage: gl_help"
  echo "  glhelp - show this help"
  echo "  glaf - print affected files"
  echo "  glla - lint all"
  echo "  glls - lint staged"
  echo "  glss - lint scss"
  echo "  glfp - force push with linting"
  echo "  gltr - regenerate translations"
  echo "  glgsc - regenerate startup css"
  echo "  rspc - shortcut for 'bundle exec rspec' ruby test"
  echo "  glww - restart gdk and tail rails web"
}

alias affectedfiles="affected_files"

alias glaf="affected_files_pretty_print"

alias glfp='force_push'

alias glla='lint_all'

alias glls='lint_staged'

alias glss='lint_css'

alias gltr='generate_translation'

alias glgsc='generate_startup_css'

alias rspc='bundle exec rspec'

alias glww='gdk restart && gdk tail rails-web'

alias glhelp='gl_help'
